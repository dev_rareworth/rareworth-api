<?php
class Category_model extends CI_Model {
    
    function __construct() {
        // Call the Model constructor
        parent::__construct(); 
        $this->load->library('password');       
    }   

    public function getCategories() {
        $this->db->select('*');
        $query = $this->db->get('categories');
        $userInfo = $query->result();	

        return $userInfo;
    }

    public function getCategory($id) {
        $query = $this->db->get_where('categories', array('id' => $id), 1);  
        if($this->db->affected_rows() > 0){
            $row = $query->row();
            unset($row->date_created);
            return $row;
        }else{
            return array();
        }
    }
}
