<?php
class User_model extends CI_Model {
    
    function __construct() {
        // Call the Model constructor
        parent::__construct(); 
        $this->load->library('password');
        $this->load->model('Image_model', 'image_model', TRUE);       
    }    


	public function insertUser($data) {  
            $query = $this->db->insert_string('users', $data);             
            $this->db->query($query);
            return $this->db->insert_id();
    }
    
    public function isDuplicate($email) {     
        $this->db->get_where('users', array('email' => $email), 1);
        return $this->db->affected_rows() > 0 ? TRUE : FALSE;         
    }

    public function isDuplicateUsername($username) {     
        $this->db->get_where('users', array('username' => $username), 1);
        return $this->db->affected_rows() > 0 ? TRUE : FALSE;         
    }
    
    public function insertToken($data) { 
    	$token = substr(sha1(rand()), 0, 30); 
        $date = $data['date_created'];
        
        $string = array(
                'access_token' => $token,
                'user_id' => $data['user_id'],
                'date_created'=> $date
            );
        $query = $this->db->insert_string('access_tokens', $string);
        $this->db->query($query);
        return $token;  
    }

    public function insertResetToken($data) { 
        $date = $data['date_created'];
        $token = $data['reset_token'];

        $string = array(
                'reset_token' => $token,
                'user_id' => $data['user_id'],
                'date_created'=> $date
            );
        $query = $this->db->insert_string('reset_password_tokens', $string);
        $this->db->query($query);
        return $token;  
    }

    public function isTokenValid($token) {
        $query = $this->db->get_where('access_tokens', array('access_token' => $token), 1);        
        if($this->db->affected_rows() > 0){
            return true;
            
        }else{
            return false;
        } 
    }  

    public function getUserInfo($id) {
        $query = $this->db->get_where('users', array('id' => $id), 1);  
        if($this->db->affected_rows() > 0){
            $row = $query->row();

            $has_password = true;
            $is_email_verified = false;   

            if($row->password == '') {
                $has_password = false;
            }

            if($row->is_email_confirmed == '1') {
                $is_email_verified = true;
            }

			$user_details = $arrayName = array('id' => $row->id, 
            	'firstName' => $row->first_name, 
            	'lastName' => $row->last_name, 
            	'email' => $row->email,
                'bio' => $row->bio,
            	'username' => $row->username,
                'birthdate' => $row->birthdate,
            	'isEmailConfirmed' => $is_email_verified,
                'haPassword' => $has_password,
            	'profilePricture' => $this->image_model->getImageInfo($row->profile_picture),
                'locationName' => $row->location_name,
                'latitude' => $row->latitude, 
                'longitude' => $row->longitude);

            return $user_details;
        }else{
            return false;
        }
    }

    public function updateUser($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('users', $data);
        return $this->getUserInfo($id);
    }

    public function getUserInfoViaToken($token) {
        $query = $this->db->get_where('access_tokens', array('access_token' => $token), 1);  
        if($this->db->affected_rows() > 0){
            $row = $query->row();

            return $this->getUserInfo($row->user_id);
        }else{
            return false;
        }
    }

    public function checkLogin($data) {             
        $this->db->select('*');
        $this->db->where('email', $data['email']);
        $query = $this->db->get('users');
        $userInfo = $query->row();
        
        if($this->db->affected_rows() < 1) { 
        	return false;
        } else {
        	if(!$this->password->validate_password($data['password'], $userInfo->password)){
            	return false; 
        	}
        
        	$this->updateLoginTime($userInfo->id);
        
        	unset($userInfo->password);
        	return $userInfo->id; 
        }
    }

    public function checkEmail($data) {             
        $this->db->select('*');
        $this->db->where('email', $data['email']);
        $query = $this->db->get('users');
        $userInfo = $query->row();
        
        if($this->db->affected_rows() < 1) { 
        	return false;
        } else {
        	return $userInfo->id; 
        }
    }
    
    public function updateLoginTime($id) {
        $this->db->where('id', $id);
        $this->db->update('users', array('last_login' => date('Y-m-d h:i:s A')));
        return;
    }

    public function getResetTokenInfo($data) {
    	$this->db->select('*');
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('reset_token', $data['reset_token']);
        $query = $this->db->get('reset_password_tokens');
        $tokenInfo = $query->row();

        if($this->db->affected_rows() < 1) { 
        	return false;
        } else {
        	return $tokenInfo; 
        }
    }

    public function updatePassword($data) {
        $this->db->where('id', $data['user_id']);
        $this->db->update('users', array('date_updated' => date('Y-m-d h:i:s A'), 'password' => $data['password']));
        return $data['user_id'];
    }

    public function checkOldPassword($data) {
         $query = $this->db->get_where('users', array('id' => $data['id']), 1);  
        if($this->db->affected_rows() > 0){
            $row = $query->row();

            if($this->password->validate_password($data['old_password'], $row->password)){
                return true; 
            } else {
                return false;
            }

        }else{
            return false;
        }
    }

    public function checkIfPasswordAvailable($id) {
         $query = $this->db->get_where('users', array('id' => $id), 1);  
        if($this->db->affected_rows() > 0){
            $row = $query->row();

            if($row->password == '' && $row->is_social_media) {
                return true;
            } else {
                return false;
            }
            
        }else{
            return false;
        }
    }
}