<?php
class Image_model extends CI_Model {
    
    function __construct() {
        // Call the Model constructor
        parent::__construct();       
    }  

    public function insertImage($data) {  
    	$image = array(
        	'filename' => $data['filename'],
            'location' => $data['location'],
            'date_created' => $data['date_created'],
            'user_id' => $data['user_id']
            );

        $query = $this->db->insert_string('images', $image);             
        $this->db->query($query);
        return $this->getImageInfo($this->db->insert_id());
    }

    public function getImageInfo($id) {
        $query = $this->db->get_where('images', array('id' => $id), 1);  
        if($this->db->affected_rows() > 0){
            $row = $query->row();

			$user_details = array('id' => $row->id, 
            	'filename' => $row->filename, 
            	'location' => $row->location, 
            	'dateCreated' => $row->date_created);

            return $user_details;
        }else{
            return array();
        }
    }

    public function getImages($imageIds) {
        $images = $imageIds;
        $images = str_replace('[', '', $images);
        $images = str_replace(']', '', $images);

        $images = explode(',', $images);
        $images_result = array();

        foreach($images as $imageId) {
            $images_result[] = $this->getImageInfo($imageId);
        }

        return $images_result;
    }
}