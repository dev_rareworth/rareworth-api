<?php
class Currency_model extends CI_Model {
    
    function __construct() {
        // Call the Model constructor
        parent::__construct();  
    }    

    public function getCurrencies() {
        $this->db->select('*');
        $query = $this->db->get('currencies');
        $currencies = $query->result();	

        $currency_return = array();
        foreach ($currencies as $currency) {
        	unset($currency->date_created);
        	$temp_currency = array('id' => $currency->id, 
            'name' => $currency->name,
            'code' => $currency->code);
        	$currency_return[] = $temp_currency;
        }

        return $currency_return;
    }

    public function getCurrency($id) {
        $query = $this->db->get_where('currencies', array('id' => $id), 1);  
        if($this->db->affected_rows() > 0){
            $currency = $query->row();

            unset($currency->date_created);
            $temp_currency = array('id' => $currency->id, 
            'name' => $currency->name,
            'code' => $currency->code);
            
            return $temp_currency;
        }else{
            return array();
        }
    }

}