<?php
class Product_model extends CI_Model {
    
    function __construct() {
        // Call the Model constructor
        parent::__construct();    
        $this->load->model('Image_model', 'image_model', TRUE); 
        $this->load->model('Category_model', 'category_model', TRUE);
        $this->load->model('Currency_model', 'currency_model', TRUE);
        $this->load->model('Condition_model', 'condition_model', TRUE);
        $this->load->model('Legal_Document_model', 'legal_document_model', TRUE);     
    }    


	public function insertProduct($data) {  
        $query = $this->db->insert_string('products', $data);             
        $this->db->query($query);
        return $this->db->insert_id();
    }

    public function updateProduct($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('products', $data);
        return $this->getProductDetails($id);
    }

    public function updateProductStatus($data, $status) {
        $date = date('Y-m-dH-i-s');

        $productsIds = $data;
        $productsIds = str_replace('[', '', $productsIds);
        $productsIds = str_replace(']', '', $productsIds);

        $productsIds = explode(',', $productsIds);
        $productIds_result = array();

        foreach($productsIds as $productId) {
            $productIds_result[] = array('id' => $productId, 'status' => $status, 'date_updated' => $date);
        }

        $this->db->update_batch('products', $productIds_result, 'id');

        return $data;
    }

     public function getProductDetails($id) {
        $query = $this->db->get_where('products', array('id' => $id), 1);  
        if($this->db->affected_rows() > 0){
            $row = $query->row();

            return $this->formatCompleProductDetails($row);
        }else{
            return false;
        }
    }

    public function getProductList($user_id, $status, $page, $perPage) {
        $this->db->limit($perPage, (($page-1) * $perPage))->select('*');
        if($status != '') {
            $this->db->where('status', $status);
        }
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('products');
        $products = $query->result();

        $product_results = array();

        foreach ($products as $product) {
            $product_results[] = $this->formatSimpleProductDetails($product);
        }
        
        return $product_results;

    }

    public function formatCompleProductDetails($row) {
        $product_details = array('id' => $row->id, 
                'productName' => $row->name, 
                'description' => $row->description, 
                'images' => $this->image_model->getImages($row->image_ids),
                'primaryImage' => $this->image_model->getImageInfo($row->primary_image_id),
                'dateAcquired' => $row->date_acquired,
                'upvote' => $row->upvote, 
                'downvote' => $row->downvote, 
                'locationName' => $row->location_name,
                'latitude' => $row->latitude,
                'longitude' => $row->longitude,
                'category' => $this->category_model->getCategory($row->category_id),
                'status' => $row->status, 
                'statusDescription' => $this->getStatusDescription($row->status),
                'currency' => $this->currency_model->getCurrency($row->currency_id),
                'value' => $row->value,
                'conditionRating' => $row->condition_rating,
                'condition' => $this->condition_model->getCondition($row->condition_id),
                'legal' => $this->legal_document_model->getLegalDocument($row->legal_id));

            return $product_details;
    }

    public function formatSimpleProductDetails($row) {
        $product_details = array('id' => $row->id, 
                'productName' => $row->name, 
                'description' => $row->description, 
                'primaryImage' => $this->image_model->getImageInfo($row->primary_image_id),
                'upvote' => $row->upvote, 
                'downvote' => $row->downvote, 
                'category' => $this->category_model->getCategory($row->category_id),
                'status' => $row->status, 
                'statusDescription' => $this->getStatusDescription($row->status),
                'currency' => $this->currency_model->getCurrency($row->currency_id),
                'value' => $row->value,
                'conditionRating' => $row->condition_rating,
                'condition' => $this->condition_model->getCondition($row->condition_id),
                'legal' => $this->legal_document_model->getLegalDocument($row->legal_id));

            return $product_details;
    }

    public function getStatusDescription($status) {
        if($status == '1') {
            return 'Active';
        } elseif ($status == '2') {
            return 'Inactive';
        } elseif ($status == '3') {
            return 'Deleted';
        } else {
            return 'Unknown Status';
        }
    }
}