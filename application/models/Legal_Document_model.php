<?php
class Legal_Document_model extends CI_Model {
    
    function __construct() {
        // Call the Model constructor
        parent::__construct();  
    }    

    public function getLegalDocuments() {
        $this->db->select('*');
        $query = $this->db->get('legals');
        $legals = $query->result();	

        $legals_return = array();
        foreach ($legals as $legal) {
        	unset($legal->date_created);
        	$temp_legal = array('id' => $legal->id, 
            'name' => $legal->description);
        	$legals_return[] = $temp_legal;
        }

        return $legals_return;
    }

    public function getLegalDocument($id) {
        $query = $this->db->get_where('legals', array('id' => $id), 1);  
        if($this->db->affected_rows() > 0){
            $legal = $query->row();

            unset($legal->date_created);
            $temp_legal = array('id' => $legal->id, 
            'name' => $legal->description);
            return $temp_legal;
        }else{
            return array();
        }
    }

}