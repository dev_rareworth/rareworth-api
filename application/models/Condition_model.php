<?php
class Condition_model extends CI_Model {
    
    function __construct() {
        // Call the Model constructor
        parent::__construct();  
    }    

    public function getConditions() {
        $this->db->select('*');
        $query = $this->db->get('conditions');
        $conditions = $query->result();	

        $conditions_return = array();
        foreach ($conditions as $condition) {
    		unset($condition->date_created);
    		$temp_condition = array('id' => $condition->id, 
        	'name' => $condition->description);
        	$conditions_return[] = $temp_condition;
        }

        return $conditions_return;
    }

    public function getCondition($id) {
        $query = $this->db->get_where('conditions', array('id' => $id), 1);  
        if($this->db->affected_rows() > 0){
            $condition = $query->row();

            unset($condition->date_created);
            $temp_condition = array('id' => $condition->id, 
            'name' => $condition->description);
            return $temp_condition;
        }else{
            return array();
        }
    }

}