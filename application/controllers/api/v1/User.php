<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User class.
 * 
 * @extends CI_Controller
 */
class User extends CI_Controller {
    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('User_model', 'user_model', TRUE);
        $this->load->model('Image_model', 'image_model', TRUE);
        $this->load->helper('email'); 
        $this->load->library('password'); 

        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_message('required', '%s is required.');
        $this->form_validation->set_message('valid_email', '%s is not a valid email.');
    }
    
    
    public function index() {
    }

    public function getUserDetails() {
        $this->form_validation->set_rules('accessToken', 'Access Token', 'required');  
        $access_token = $this->input->post('accessToken');

        if ($this->form_validation->run() == FALSE) {   

            $error_message = validation_errors();

            return generateResponse(200, false, $error_message, '');

        } else{  
            $user_details = $this->user_model->getUserInfoViaToken($access_token);

            if ($user_details === false) {
                return generateResponse(200, false, 'User not found!', '');
            } else {
                // array_pop($user_details['password']);
                return generateResponse(200, true, 'User found!', $user_details);
            }
        }
    }

    public function register() {

        $this->form_validation->set_rules('password', 'Password', 'required');    
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email'); 

        if ($this->form_validation->run() == FALSE) {   

            $error_message = validation_errors();

            return generateResponse(200, false, $error_message, '');

        } else{  

            if($this->user_model->isDuplicate($this->input->post('email'))) { 

                return generateResponse(200, false, 'Email address already exists!', '');

            } else {

                $email = $this->input->post('email');
                $password = $this->password->create_hash($this->input->post('password'));
                $date = date('Y-m-d H:i:s');
                $is_social_media = $this->input->post('isSocialMedia');

                if($is_social_media == true) {
                    $is_social_media = 1;
                } else {
                    $is_social_media = 0;
                }

                $params = array('email' => $email, 
                    'password' => $password, 
                    'last_login' => $date,
                    'date_created' => $date,
                    'date_updated' => $date,
                    'is_social_media' => $is_social_media);

                $clean = $this->security->xss_clean($params);
                $id = $this->user_model->insertUser($clean); 

                $params = array('user_id' => $id,
                    'date_created' => $date);

                $access_token = $this->user_model->insertToken($params);

                return generateResponse(200, true, 'You have successfully registered!', array('id' => $id, 'access_token' => $access_token));
            }

        }
    }

    public function updateProfile() {
        $this->form_validation->set_rules('lastName', 'Last Name', 'required'); 
        $this->form_validation->set_rules('firstName', 'First Name', 'required'); 
        $this->form_validation->set_rules('bio', 'Bio', 'required'); 
        $this->form_validation->set_rules('username', 'Username', 'required'); 
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email'); 


        $access_token = $this->input->post('accessToken');
        $last_name = $this->input->post('lastName');
        $first_name = $this->input->post('firstName');
        $bio = $this->input->post('bio');
        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $profile_picture = $this->input->post('profilePicture');
        $birthdate = $this->input->post('birthdate');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        $location_name = $this->input->post('locationName');

        if ($this->form_validation->run() == FALSE) {   

            $error_message = validation_errors();
            return generateResponse(200, false, $error_message, '');

        } else{  
            $user_details = $this->user_model->getUserInfoViaToken($access_token);

            if ($user_details === false) {
                return generateResponse(200, false, 'User not found!\nInvalid access token.', '');
            } else {
                $error_message = '';
                if($user_details['email'] != $email) {
                    if($this->user_model->isDuplicate($email)) { 
                        $error_message = 'Email address already exists!';
                    } 
                }

                if($user_details['username'] != $username) {
                    if($this->user_model->isDuplicateUsername($username)) { 
                        $error_message = 'Username already exists!';
                    } 
                }

                if($profile_picture != '') {
                    if($this->image_model->getImageInfo($profile_picture) == false) {
                        $error_message = 'Image not found!';
                    }
                }

                if($error_message != '') {
                    return generateResponse(200, false, $error_message, '');
                } else {
                    $date = date('Y-m-d H:i:s');

                    $params = array('email' => $email, 
                    'first_name' => $first_name, 
                    'last_name' => $last_name, 
                    'bio' => $bio, 
                    'username' => $username, 
                    'birthdate' => $birthdate,
                    'latitude' => $latitude, 
                    'longitude' => $longitude,
                    'location_name' => $location_name,
                    'date_updated' => $date);

                    if($profile_picture != '') {
                        $params['profile_picture'] = $profile_picture;
                    }

                    $clean = $this->security->xss_clean($params);
                    $user = $this->user_model->updateUser($clean, $user_details['id']); 
                    return generateResponse(200, true, 'Successfully updated profile!', $user);
                }
            }
        }
    }


    public function login() {
        $this->form_validation->set_rules('password', 'Password', 'required');    
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email'); 

        if ($this->form_validation->run() == FALSE) {   

            $error_message = '';

            $email = $this->input->post('email');
            $password = $this->input->post('password');

            if (empty($email) AND empty($password)) {
                $error_message = 'Complete all fields!';
            } else if (empty($email)) {
                $error_message = 'Email Address is required!';
            } else if (empty($password)) {
                $error_message = 'Password is required!';
            } else if (!valid_email($email)) {
                $error_message = 'Email Address is invalid!';
            }

            return generateResponse(200, false, $error_message, '');

        } else{ 

            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $date = date('Y-m-d H:i:s');

            $params = array('email' => $email, 
                'password' => $password,                     
                'last_login' => $date);

            $clean = $this->security->xss_clean($params);
            $id = $this->user_model->checkLogin($clean); 

            if($id === false) {
                return generateResponse(200, false, 'Incorrect email/password.', array());
            } else {
                $params = array('user_id' => $id,
                'date_created' => $date);
                 $access_token = $this->user_model->insertToken($params);
                 return generateResponse(200, true, 'You have successfully login', array('id' => $id, 'access_token' => $access_token));
            }
        }
    }

    public function loginViaSocialMedia() {
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email'); 

        if ($this->form_validation->run() == FALSE) {   

            $error_message = '';

            $email = $this->input->post('email');

            if (empty($email)) {
                $error_message = 'Email Address is required!';
            } else if (!valid_email($email)) {
                $error_message = 'Email Address is invalid!';
            }

            return generateResponse(200, false, $error_message, '');

        } else{ 

            $email = $this->input->post('email');
            $date = date('Y-m-d H:i:s');

            $params = array('email' => $email,                     
                'last_login' => $date);

            $clean = $this->security->xss_clean($params);
            $id = $this->user_model->checkEmail($clean); 

            if($id === false) {
                return generateResponse(200, true, 'Email address doesn\'t exists.', array('access_token' => null));
            } else {
                $params = array('user_id' => $id,
                'date_created' => $date);
                 $access_token = $this->user_model->insertToken($params);
                 return generateResponse(200, true, 'You have successfully login', array('access_token' => $access_token));
            }
        }

    }

    public function checkAccessToken() {
        $valid = $this->user_model->isTokenValid($this->input->post('access_token'));
        if($valid === true) {
            return generateResponse(200, $valid, 'Access Token is valid.', '');
        } else {
            return generateResponse(200, $valid, 'Access Token is invalid.', '');
        }
    }


    public function sendResetPasswordToken() {
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email'); 

        if ($this->form_validation->run() == FALSE) {   

            $error_message = '';

            $email = $this->input->post('email');

            if (empty($email)) {
                $error_message = 'Email Address is required!';
            } else if (!valid_email($email)) {
                $error_message = 'Email Address is invalid!';
            }

            return generateResponse(200, false, $error_message, '');

        } else{ 

            $email = $this->input->post('email');
            $params = array('email' => $email);

            $clean = $this->security->xss_clean($params);
            $id = $this->user_model->checkEmail($clean); 

            if($id === false) {
                return generateResponse(200, false, 'Email address doesn\'t exists.', '');
            } else {
                $reset_token = mt_rand(100000, 999999);
                $date = date('Y-m-d H:i:s');
                $params = array('user_id' => $id,
                'date_created' => $date,
                'reset_token' => $reset_token);
                 $reset_token = $this->user_model->insertResetToken($params);
                 return $this->sendEmail($reset_token, $email);
            }
        }
    }

    public function resetPassword() {

        $this->form_validation->set_rules('password', 'Password', 'required');  
        $this->form_validation->set_rules('resetToken', 'Reset Token', 'required');    
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email'); 

        if ($this->form_validation->run() == FALSE) {   

            $error_message = validation_errors();
            return generateResponse(200, false, $error_message, '');

        } else{  

            $email = $this->input->post('email');
            $reset_token = $this->input->post('resetToken');
            $password = $this->password->create_hash($this->input->post('password'));
            $params = array('email' => $email);
            $clean = $this->security->xss_clean($params);
            $id = $this->user_model->checkEmail($clean); 

            if($id === false) {
                return generateResponse(200, false, 'Email address doesn\'t exists.', '');
            } else {
                $params = array('user_id' => $id,
                'reset_token' => $reset_token);
                 $reset_token_info = $this->user_model->getResetTokenInfo($params);

                if($reset_token_info === false) {
                    return generateResponse(200, false, 'Incorrect reset token.', '');
                } else {
                    $params = array('user_id' => $id,
                    'password' => $password);
                    $user_id = $this->user_model->updatePassword($params);

                    if($user_id == $id) {
                        return generateResponse(200, true, 'Password successfully changed.', '');
                    } else {
                        return generateResponse(200, false, 'Something went wrong', '');
                    }
                }
            }
        }
    }

    public function changePassword() {

        $this->form_validation->set_rules('accessToken', 'Access Token', 'required');  
        $this->form_validation->set_rules('oldPassword', 'Old Password', 'required');  
        $this->form_validation->set_rules('newPassword', 'New Password', 'required');  

        if ($this->form_validation->run() == FALSE) {   

            $error_message = validation_errors();
            return generateResponse(200, false, $error_message, '');

        } else{  
            $access_token = $this->input->post('accessToken');
            $old_password = $this->input->post('oldPassword');
            $new_password = $this->password->create_hash($this->input->post('newPassword'));

            $user_details = $this->user_model->getUserInfoViaToken($access_token);

            if ($user_details === false) {
                return generateResponse(200, false, 'User not found!\nInvalid access token.', '');
            } else {
                $result = $this->user_model->checkOldPassword(array('id' => $user_details['id'], 'old_password' => $old_password));

                if($result === true) {
                    $params = array('user_id' => $user_details['id'],
                    'password' => $new_password);
                    $user_id = $this->user_model->updatePassword($params);

                    if($user_id == $user_details['id']) {
                        return generateResponse(200, true, 'Password successfully changed.', '');
                    } else {
                        return generateResponse(200, false, 'Something went wrong', '');
                    }
                } else {
                    return generateResponse(200, false, 'Incorrect old password', array());
                }
            }
        }
    }

    public function createPassword() {

        $this->form_validation->set_rules('accessToken', 'accessToken', 'required');  
        $this->form_validation->set_rules('newPassword', 'New Password', 'required'); 

        if ($this->form_validation->run() == FALSE) {   

            $error_message = validation_errors();
            return generateResponse(200, false, $error_message, '');

        } else{  
            $access_token = $this->input->post('accessToken');
            $password = $this->password->create_hash($this->input->post('newPassword'));

            $user_details = $this->user_model->getUserInfoViaToken($access_token);

            if ($user_details === false) {
                return generateResponse(200, false, 'User not found!\nInvalid access token.', '');
            } else {

                $result = $this->user_model->checkIfPasswordAvailable($user_details['id']);

                if($result === true) {
                    $params = array('user_id' => $user_details['id'],
                    'password' => $password);
                    $user_id = $this->user_model->updatePassword($params);

                    if($user_id == $user_details['id']) {
                        return generateResponse(200, true, 'Password successfully created.', '');
                    } else {
                        return generateResponse(200, false, 'Something went wrong', '');
                    }
                } else {
                    return generateResponse(200, false, 'Error in creating password. Already have a password.', array());
                }
            }
        }
    }

    public function sendEmail($reset_token, $email) {

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://sg2plcpnl0161.prod.sin2.secureserver.net',
            'smtp_port' => 465,
            'smtp_user' => 'no-reply@rareworth.com',
            'smtp_pass' => 'r@rew0rth',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1');

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        
        $this->email->from('no-reply@rareworth.com', 'RareWorth');
        $this->email->to($email);
        $this->email->subject('Reset Password');
        $this->email->message('Your Reset Token is ' . $reset_token);
        if ($this->email->send())
            return generateResponse(200, true, 'Reset token has been sent to your email. Check your email.', '');
        else
            return generateResponse(200, false, 'Something went wrong. Try again.', '');
    }
}