<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Currency extends CI_Controller {
    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Currency_model', 'currency_model', TRUE);
    }

    public function getCurrencies() {
        $currencies = $this->currency_model->getCurrencies();

        if ($currencies === false) {
            return generateResponse(200, false, 'No currency found!', '');
        } else {
            return generateResponse(200, true, 'List of currencies.', $currencies);
        }
    }
}
    