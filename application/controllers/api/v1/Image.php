<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Image extends CI_Controller {
	/**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Image_model', 'image_model', TRUE);
        $this->load->model('User_model', 'user_model', TRUE);
        $this->load->helper('url');

        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_message('required', '%s is required.');
    }

    public function upload() {
        $this->form_validation->set_rules('accessToken', 'Access Token', 'required');  
        $access_token = $this->input->post('accessToken');

        if ($this->form_validation->run() == FALSE) {   

            $error_message = validation_errors();
            return generateResponse(200, false, $error_message, '');

        } else{  
            $user_details = $this->user_model->getUserInfoViaToken($access_token);

            if ($user_details === false) {
                return generateResponse(200, false, 'User not found!', '');
            } else {
                $date = date('Y-m-dH-i-s');
                $file_name = substr(sha1(rand()), 0, 10).$date;

                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'jpg|png';
                $config['file_name'] = $file_name;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('image')) {
                    $upload_error = array('error' => $this->upload->display_errors());
                    return generateResponse(200, false, 'Error in uploading image!', $upload_error);
                }   else {
                    $upload_data = $this->upload->data();

                    $date = date('Y-m-d H:i:s');

                    $params = array('filename' => $upload_data['file_name'], 
                        'location' => $this->config->base_url().'uploads/'.$upload_data['file_name'],
                        'date_created' => $date,
                        'user_id' => $user_details['id']);

                    $clean = $this->security->xss_clean($params);
                    $image = $this->image_model->insertImage($clean); 

                    return generateResponse(200, true, 'Image successfully uploaded!', $image);
                }
            }
        }
    }
}