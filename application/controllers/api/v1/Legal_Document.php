<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Legal_Document extends CI_Controller {
    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        
        parent::__construct();
        $this->load->model('Legal_Document_model', 'legal_document_model', TRUE);
    }

    public function getLegalDocuments() {
        $legals = $this->legal_document_model->getLegalDocuments();

        if ($legals === false) {
            return generateResponse(200, false, 'No legal document found!', '');
        } else {
            return generateResponse(200, true, 'List of legal document.', $legals);
        }
    }
}
    