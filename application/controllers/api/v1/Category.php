<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Category extends CI_Controller {
	/**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('Category_model', 'category_model', TRUE);
    }

    public function getCategories() {
        $response = $this->category_model->getCategories();
        if($response === false) {
            return generateResponse(200, false, 'No categories found', '');
        } else {
            return generateResponse(200, true, 'Succesfully found categories.', $response);
        }
    }
}