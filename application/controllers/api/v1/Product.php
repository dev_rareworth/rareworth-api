<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Product extends CI_Controller {
    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        
        parent::__construct();

        $this->load->library('form_validation');

        $this->load->model('Product_model', 'product_model', TRUE);
        $this->load->model('User_model', 'user_model', TRUE);
        $this->load->model('Category_model', 'category_model', TRUE);
        $this->load->model('Currency_model', 'currency_model', TRUE);
        $this->load->model('Condition_model', 'condition_model', TRUE);
        $this->load->model('Legal_Document_model', 'legal_document_model', TRUE);

        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_message('required', '%s is required.');
        $this->form_validation->set_message('valid_email', '%s is not a valid email.');
    }

    public function getProductDetails() {
        $this->form_validation->set_rules('productId', 'Product ID', 'required');
        $this->form_validation->set_rules('accessToken', 'Access Token', 'required');

        if ($this->form_validation->run() == FALSE) {   

            $error_message = validation_errors();
            return generateResponse(200, false, $error_message, '');

        } else{ 
            $access_token = $this->input->post('accessToken');
            $product_id = $this->input->post('productId');
            $user_details = $this->user_model->getUserInfoViaToken($access_token);

            if ($user_details === false) {
                return generateResponse(200, false, 'User not found!', null);
            } else {
                $prodduct_details = $this->product_model->getProductDetails($product_id);

                if ($prodduct_details === false) {
                    return generateResponse(200, false, 'Product not found!', null);
                } else {
                return generateResponse(200, true, 'Product found!', $prodduct_details);
                }
            }
        }
    }

    public function getProductList() {
        $this->form_validation->set_rules('accessToken', 'Access Token', 'required');

        if ($this->form_validation->run() == FALSE) {   

            $error_message = validation_errors();
            return generateResponse(200, false, $error_message, '');

        } else{ 
            $access_token = $this->input->post('accessToken');
            $status = $this->input->post('status');
            $page = $this->input->post('page');
            $per_page = $this->input->post('perPage');
            $user_details = $this->user_model->getUserInfoViaToken($access_token);

            if ($user_details === false) {
                return generateResponse(200, false, 'User not found!', null);
            } else {

                if($page == '') {
                    $page = 1;
                }
                if($per_page == '') {
                    $per_page = 10;
                }

                $prodduct_details = $this->product_model->getProductList($user_details['id'], $status, $page, $per_page);

                if ($prodduct_details === false) {
                    return generateResponse(200, false, 'Products not found!', null);
                } else {
                return generateResponse(200, true, 'Products found!', $prodduct_details);
                }
            }
        }
    }

    public function getProductAttributes() {
        $categories = $this->category_model->getCategories();
        $currencies = $this->currency_model->getCurrencies();
        $conditions = $this->condition_model->getConditions();
        $legals = $this->legal_document_model->getLegalDocuments();

        // if ($categories === false || $currencies === false || $conditions === false || $legals === false) {
        //     return generateResponse(200, false, 'Something went wrong!', '');
        // } else {
            return generateResponse(200, true, 'Product Attributes.', 
            array('categories' => $categories, 
                'currencies' => $currencies,
                'conditions' => $conditions,
                'legals' => $legals));
        // }
    }

    public function upload() {
        $this->form_validation->set_rules('productName', 'Product Name', 'required');
        $this->form_validation->set_rules('accessToken', 'Access Token', 'required');
        $this->form_validation->set_rules('imageIds', 'Product Images', 'required');
        $this->form_validation->set_rules('primaryImageId', 'Primary Image', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('dateAcquired', 'Date acquired', 'required');
        $this->form_validation->set_rules('categoryId', 'Category', 'required');
        $this->form_validation->set_rules('currencyId', 'Currency', 'required');
        $this->form_validation->set_rules('value', 'Product Value', 'required');
        $this->form_validation->set_rules('conditionRating', 'Condition Rating', 'required');
        $this->form_validation->set_rules('conditionId', 'Condition', 'required');
        $this->form_validation->set_rules('legalId', 'Legal Document', 'required');


        if ($this->form_validation->run() == FALSE) {   

            $error_message = validation_errors();
            return generateResponse(200, false, $error_message, '');

        } else{ 
            $access_token = $this->input->post('accessToken');
            $user_details = $this->user_model->getUserInfoViaToken($access_token);

            if ($user_details === false) {
                return generateResponse(200, false, 'User not found!', '');
            } else {
                $date = date('Y-m-dH-i-s');
                $user_id = $user_details['id'];
                $product_name = $this->input->post('productName');
                $description = $this->input->post('description');
                $image_ids = $this->input->post('imageIds');
                $primary_image_id = $this->input->post('primaryImageId');
                $date_acquired = $this->input->post('dateAcquired');
                $location_name = $this->input->post('locationName');
                $latitude = $this->input->post('latitude');
                $longitude = $this->input->post('longitude');
                $category_id = $this->input->post('categoryId');
                $status = '1';
                $currency_id = $this->input->post('currencyId');
                $value = $this->input->post('value');
                $condition_rating = $this->input->post('conditionRating');
                $condition_id = $this->input->post('conditionId');
                $legal_id = $this->input->post('legalId');

                $params = array('name' => $product_name, 
                    'image_ids' => $image_ids, 
                    'primary_image_id' => $primary_image_id, 
                    'description' => $description, 
                    'status' => $status, 
                    'date_acquired' => $date_acquired,
                    'location_name' => $location_name, 
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'user_id' => $user_id,
                    'currency_id' => $currency_id,
                    'value' => $value,
                    'condition_rating' => $condition_rating,
                    'condition_id' => $condition_id,
                    'category_id' => $category_id,
                    'legal_id' => $legal_id,
                    'date_updated' => $date,
                    'date_created' => $date);

                $clean = $this->security->xss_clean($params);
                $id = $this->product_model->insertProduct($clean);

                return generateResponse(200, true, 'You have successfully uploaded a product!', array('id' => $id));

            }
        }
    }

    public function edit() {
        $this->form_validation->set_rules('productId', 'Product ID', 'required');
        $this->form_validation->set_rules('productName', 'Product Name', 'required');
        $this->form_validation->set_rules('accessToken', 'Access Token', 'required');
        $this->form_validation->set_rules('imageIds', 'Product Images', 'required');
        $this->form_validation->set_rules('primaryImageId', 'Primary Image', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('dateAcquired', 'Date acquired', 'required');
        $this->form_validation->set_rules('categoryId', 'Category', 'required');
        $this->form_validation->set_rules('currencyId', 'Currency', 'required');
        $this->form_validation->set_rules('value', 'Product Value', 'required');
        $this->form_validation->set_rules('conditionRating', 'Condition Rating', 'required');
        $this->form_validation->set_rules('conditionId', 'Condition', 'required');
        $this->form_validation->set_rules('legalId', 'Legal Document', 'required');


        if ($this->form_validation->run() == FALSE) {   

            $error_message = validation_errors();
            return generateResponse(200, false, $error_message, '');

        } else{ 
            $access_token = $this->input->post('accessToken');
            $user_details = $this->user_model->getUserInfoViaToken($access_token);

            if ($user_details === false) {
                return generateResponse(200, false, 'User not found!', '');
            } else {
                $date = date('Y-m-dH-i-s');
                $user_id = $user_details['id'];
                $product_id = $this->input->post('productId');
                $product_name = $this->input->post('productName');
                $description = $this->input->post('description');
                $image_ids = $this->input->post('imageIds');
                $primary_image_id = $this->input->post('primaryImageId');
                $date_acquired = $this->input->post('dateAcquired');
                $location_name = $this->input->post('locationName');
                $latitude = $this->input->post('latitude');
                $longitude = $this->input->post('longitude');
                $category_id = $this->input->post('categoryId');
                $status = '1';
                $currency_id = $this->input->post('currencyId');
                $value = $this->input->post('value');
                $condition_rating = $this->input->post('conditionRating');
                $condition_id = $this->input->post('conditionId');
                $legal_id = $this->input->post('legalId');

                $params = array('name' => $product_name, 
                    'image_ids' => $image_ids, 
                    'primary_image_id' => $primary_image_id, 
                    'description' => $description, 
                    'status' => $status, 
                    'date_acquired' => $date_acquired,
                    'location_name' => $location_name, 
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'user_id' => $user_id,
                    'currency_id' => $currency_id,
                    'value' => $value,
                    'condition_rating' => $condition_rating,
                    'condition_id' => $condition_id,
                    'category_id' => $category_id,
                    'legal_id' => $legal_id,
                    'date_updated' => $date);

                $clean = $this->security->xss_clean($params);
                $id = $this->product_model->updateProduct($clean, $product_id);

                return generateResponse(200, true, 'You have successfully uploaded a product!', array('id' => $id));

            }
        }
    }

    public function updateStatus() {
        $this->form_validation->set_rules('productIds', 'Product IDs', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');

        if ($this->form_validation->run() == FALSE) {   

            $error_message = validation_errors();
            return generateResponse(200, false, $error_message, '');

        } else{ 
            $access_token = $this->input->post('accessToken');
            $product_ids = $this->input->post('productIds');
            $status = $this->input->post('status');

            if($status == '1' || $status == '2' || $status == '3' || $status == 1 || $status == 2 || $status == 3) {
                $user_details = $this->user_model->getUserInfoViaToken($access_token);

                if ($user_details === false) {
                    return generateResponse(200, false, 'User not found!', '');
                } else {
                    $products = $this->product_model->updateProductStatus($product_ids, $status);

                    $message = '';

                    if($status == '1' || $status == 1) {
                        $message = 'You have successfully move the products to Active!';
                    } 

                    if($status == '2' || $status == 2) {
                        $message = 'You have successfully move the products to Inactive!';
                    } 

                    if($status == '3' || $status == 3) {
                        $message = 'You have successfully move the products to Deleted!';
                    } 

                    return generateResponse(200, true, $message, $products);

                }
            } else {
                return generateResponse(200, false, 'Status not found!', $status);
            }
            
        }
    }

}