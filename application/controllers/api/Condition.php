<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Condition extends CI_Controller {
    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        
        parent::__construct();
        $this->load->model('Condition_model', 'condition_model', TRUE);
    }

    public function getConditions() {
        $conditions = $this->condition_model->getConditions();

        if ($conditions === false) {
            return generateResponse(200, false, 'No condition found!', '');
        } else {
            return generateResponse(200, true, 'List of conditions.', $conditions);
        }
    }
}
    