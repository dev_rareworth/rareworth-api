<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

	function generateResponse($status_header, $is_successful, $message, $data) {

		$ci=& get_instance();

        $temp_data = $data;
        if ($data == null) {
            $temp_data = array();
        }

        return $ci->output
            ->set_content_type('application/json')
            ->set_status_header($status_header)
            ->set_output(json_encode(array(
                    'isSuccessful' => $is_successful,
                    'message' => $message,
                    'data' => $temp_data
            )));
    }

